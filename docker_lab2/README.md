 # Create new docker network

 `docker network create devops`
 
  # Create 3 containers

  `docker run -d  -v /etc/docker/mnt/:/tmp --network devops --name ubuntu_date ubuntu_with_date`  

  `docker run -d -p 82:80 -v /etc/docker/mnt/:/usr/share/nginx/html --network devops --name nginx_second nginx`

  `docker run -p 83:80 -d --network devops --name nginx_third nginx_third`

 >
 >  Dockerfiles, configfiles and necessary script are uploaded to docker_lab2. 
 >
 
 **NOTE**

 > All files in mount point must be  created
