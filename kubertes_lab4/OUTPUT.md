
 # Heketi check cluster 

```bash 
[root@heketi ~]# heketi-cli cluster info b93147319086990b1c29ea22031eefdc
Cluster id: b93147319086990b1c29ea22031eefdc
Nodes:
69e3d16eb1d33e5dba3aa8342145fa8c
9c32d4a3277444a5b12a6077e6b39cb7
Volumes:

Block: true

File: true

 [root@heketi ~]# heketi-cli node list
Id:69e3d16eb1d33e5dba3aa8342145fa8c     Cluster:b93147319086990b1c29ea22031eefdc
Id:9c32d4a3277444a5b12a6077e6b39cb7     Cluster:b93147319086990b1c29ea22031eefdc


  [root@heketi ~]# heketi-cli node info 69e3d16eb1d33e5dba3aa8342145fa8c
Node Id: 69e3d16eb1d33e5dba3aa8342145fa8c
State: online
Cluster Id: b93147319086990b1c29ea22031eefdc
Zone: 1
Management Hostname: gluster1
Storage Hostname: 172.16.208.161
Devices:
Id:37c67565b4723b9d412bbfcc7c6b2aa0   Name:/dev/sdb            State:online    Size (GiB):4       Used (GiB):0       Free (GiB):4       Bricks:0

  [root@heketi ~]# heketi-cli node info 9c32d4a3277444a5b12a6077e6b39cb7
Node Id: 9c32d4a3277444a5b12a6077e6b39cb7
State: online
Cluster Id: b93147319086990b1c29ea22031eefdc
Zone: 1
Management Hostname: gluster2
Storage Hostname: 172.16.208.164
Devices:
Id:e46d035c389cd0f1a41c44e10208cff0   Name:/dev/sdb            State:online    Size (GiB):4       Used (GiB):0       Free (GiB):4       Bricks:0

```


