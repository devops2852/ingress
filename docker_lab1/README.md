## For upload to Nexus Repository with HTTP 

 - create file /etc/docker/daemon.json 
add line
``` 
{ "insecure-registries":["hostname:8686"] }
``` 
 - create second file /etc/default/docker
add line
```
DOCKER_OPTS="--config-file=/etc/docker/daemon.json"
```
 - restart docker
```
systemctl restart docker
```

## Create container with sonatype/nexus3 

```
docker run -d -p 80:8081 -p 8686:8686 sonatype/nexus3
```
`80:8081`  is web page port
`8686:8686` is my repository port 

## Create sample nginx image which will show you "Hi from container"

 - create and configure Dockerfile with content
```
FROM nginx
RUN echo 'Hi from contaner' > /usr/share/nginx/html/index.html
```
 - create image from Dockerfile

```
docker build -t custom_nginx:v1
```
## Create nginx container 

```
docker run -d -p 8080:80 custom_nginx:v1
```
## Upload nginx image to Nexus repository

```
docker tag custom_nginx:v1 hostname:8686/devops_ingress/custom_nginx:v2

docker login hostname:8686

docker push hostname:8686/devops_ingress/custom_nginx:v2
```

