 # Create new ns artifacts 

```bash
kubectl create namespace artifacts
```

 # Deployment must work in master nodes only

```bash
kubectl label nodes hostname type=master

kubectl taint nodes hostname  node-role.kubernetes.io/control-plane:NoSchedule-
```
 - For deployment see attachemnt sonatypedepl.yml 

 # Install and configure metric server

```bash

kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

kubectl edit deployments.apps -n kube-system metrics-server

```
 - add this configs: 

`--kubelet-insecure-tls`

and under `dnsPolicy: ClusterFirst` add `hostNetwork: true`


 # kubectl top node 

```bash

kubectl top pods
NAME                                 CPU(cores)   MEMORY(bytes)
nexus-66c6f64f7b-f2kfd               7m           1865Mi
nginx-deployment-5fbdf85c67-7ksrx    0m           3Mi
nginx-deployment2-5fbdf85c67-2d5ts   0m           3Mi


kubectl top nodes
NAME            CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
atest02   179m         8%     3022Mi          82%
btest02   214m         5%     4657Mi          81%
```
