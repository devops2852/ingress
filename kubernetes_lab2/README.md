
  # Create namespace named nexus

```bash
kubectl create namespace nexus
```

  # Generate deploymet manifest file with nexus namespace 

 - See attachment deployment_nexus.yml file

  # Create Nodeport type service for deployment

 - See attachemnt svc.yml file

  # Login to sonatype, create Docker proxy and Docker hosted type
              repository with anonimous access

- Docker proxy: use 8484 port and remote sotrage are http://hostname:30002
- Docker hosted: use 8585 port 

  # Push nginx image with docker to sonatype Docker repo 

```bash
 docker push hostname:30002/shahmir/custom_nginx:v3
```
  # Create nginx pod using image over sonatype using proxy repo 
     and configure  /etc/containerd/config.toml to pull with HTTP 

 - see attachement deployment_nexus2.yaml and config.toml

 - added line in .toml 

```bash
 [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."hostname:30003"]
          endpoint = ["http://hostname:30003"]

      [plugins."io.containerd.grpc.v1.cri".registry.configs]
        [plugins."io.containerd.grpc.v1.cri".registry.configs."hostname:30003".tls]
           insecure_skip_verify = true
```
