 # Install kubelet, kubeadm and kubectl

 ```bash
yum -y update && sudo systemctl 

```
 - Create and edit /etc/yum.repos.d/kubernetes.repo 


```bash
   setenforce 0
   sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

   yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

   systemctl enable --now kubelet

``` 
 - Check kubeadm version 

 - Turn off swap: 

```bash
 sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
 swapoff -a
```
 - Configure sysctl

```bash
modprobe overlay
modprobe br_netfilter

sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sysctl --system
```
 # Install Container runtime

 - Using Containerd

```bash

# Configure persistent loading of modules
sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

# Load at runtime
sudo modprobe overlay
sudo modprobe br_netfilter

# Ensure sysctl params are set
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

# Reload configs
sudo sysctl --system

# Install required packages
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

# Add Docker repo
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install containerd
sudo yum update -y && yum install -y containerd.io

# Configure containerd and start service
sudo mkdir -p /etc/containerd
sudo containerd config default > /etc/containerd/config.toml

# restart containerd
sudo systemctl restart containerd
sudo systemctl enable containerd
```
 # Configure Firewalld

```bash
systemctl disable --now firewalld
```

 #Initialize your control-plane node

```bash 
lsmod | grep br_netfilter
br_netfilter           22256  0 
bridge                151336  2 br_netfilter,ebtable_broute

systemctl enable kubelet 

kubeadm config images pull

kubeadm init

export KUBECONFIG=/etc/kubernetes/admin.conf
```

 # Install network plugin

```bash
curl https://docs.projectcalico.org/manifests/calico.yaml -O
kubectl apply -f calico.yaml
kubectl get pods --all-namespaces
```

 # If you are using master and worker on same node
 

```bash
kubectl describe nodes hostname

kubectl taint nodes rasimpgtest02  node-role.kubernetes.io/control-plane:NoSchedule-
```

 # Create .yml file for deployement

```bash
kubectl create deployment nginx-deployment --image nginx --dry-run=client  -o yaml > deployment.yaml
kubectl apply -f deployment.yaml
```

 # Edit deployment.yaml and create new deployment with deployment2.yml

```bash
kubectl apply -f deployment2.yaml

kubectl get deployment
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment    1/1     1            1           52m
nginx-deployment2   1/1     1            1           51m

kubectl get pods
NAME                                 READY   STATUS    RESTARTS   AGE
nginx-deployment-5fbdf85c67-h5zd7    1/1     Running   0          52m
nginx-deployment2-5fbdf85c67-xdk94   1/1     Running   0          51m
```
